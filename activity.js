// Find courses whose instructor is "Sir Rome" and is greater than or equal to 20000
db.courses.find({
    $and: [
        { instructor: { $regex: "Sir Rome" } },
        { price: { $gte: 20000 } }
    ]
})

// Find courses whose instructor is "Ma'am Tine" and show only its name.
db.courses.find({
    instructor: "Ma'am Tine"
},{
    name:1,
    _id:0
})

// Find courses whose instructor is "Ma'am Miah" and show only its name.
db.courses.find({
    instructor: "Ma'am Miah"
},{
    name:1,
    price:1,
    _id:0
})

// Find courses with letter 'r' in its name and has a price greater than or equal to 20000
db.courses.find({
    $and: [
        { name: { $regex: 'r', $options: '$i'} },
        { price: { $gte: 20000 } }
    ]
})

// Find courses which isActive field is true and has a price less than or equal to 25000
db.courses.find({
    $and: [
        { isActive: true },
        { price: { $lte: 25000 } }
    ]
})